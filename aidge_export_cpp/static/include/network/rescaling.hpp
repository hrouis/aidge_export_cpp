#ifndef __AIDGE_EXPORT_CPP_NETWORK_RESCALING__
#define __AIDGE_EXPORT_CPP_NETWORK_RESCALING__

// For this demo
#define SUM_T float
#define NB_BITS -32

struct NoScaling {
    SUM_T operator()(SUM_T weightedSum, unsigned int /*output*/) const {
        return weightedSum;
    }
};


#endif  // __AIDGE_EXPORT_CPP_NETWORK_RESCALING__
